I did not plan to share this code.

## What works right now:

- Led control with DMX (ola)
- relative focus control
- Full 12 led capture and save in outputDir.
- Launch with docker



# Dome control and camera capture with crappy GUI.

### Launch command (not safe,  because of --net=host)

docker run -it --rm -e DISPLAY -v /tmp.X11-unix:/tmp.X11-unix -v /home/louis/p/brdf-dome-calibration:/opt/project --volume /home/louis/ms/BRDF-Dome:/opt/capture --volume=$XAUTHORITY:/home/developer/.Xauthority --net=host --privileged -v /dev/bus/usb:/dev/bus/usb lpasselin/dome-capture

You should see DMX led control on http://127.0.0.1:9010

Focus GUI should appear (tested only on linux)




------------------------------------

# Information not required if using our setup with docker:

## Dependencies

#### gPhoto2 and libgphoto2

see requirements.txt


#### Open Lighting Architecture DMX control

Using OLA is the easiest way to use the Enttec Open DMX USB device.

Build OLA using --enable-python-libs (on arch linux: yaourt ola-git)

launch web server using "olad -l 3", go to localhost:9090 and disable all plugins (use beta UI) (or disable manually in ~/.ola/*) **except ola-ftdmx.conf**

Create universe 1 on FT probe on web page (or modify directly ~/.ola/ola-universe.conf)

Add ola/python/ola folder to PYTHONPATH

# disable USBSERIAL and enable FTDIDMX
#
ola_plugin_state -p 5 --state disable
ola_plugin_state -p 13 --state enable

" > entry_point3 && chmod +x entry_point3 && /bin/bash entry_point3
