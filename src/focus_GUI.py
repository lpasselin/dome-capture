from camera import Camera
from LED_controller import LedController
from tkinter import Tk, Canvas, Button, Entry
from PIL import ImageTk, Image
from time import sleep
import os

root = Tk()
c = Camera()
led = LedController()

IMG_DOWNLOAD_DIR = "img_dump/"

def capture_and_display_preview():
    img = Image.open(c.capture_preview())
    canvas.image = ImageTk.PhotoImage(img)
    canvas.create_image(0, 0, image=canvas.image, anchor='nw')


def focus_reset_to_index():
    c.focus(index=int(focus_index.get()))
    capture_and_display_preview()


def button_focus_near():
    c.focus_offset("Near", int(focus_offset.get()))
    capture_and_display_preview()

def button_focus_far():
    c.focus_offset("Far", int(focus_offset.get()))
    capture_and_display_preview()

def button_capture_all_lights():
    # led.all_off()

    output_dir = IMG_DOWNLOAD_DIR + directory_name.get() + "/"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(1, 13):
        led[i] = 255
        sleep(0.3)
        c.capture(filename= output_dir + "light_{:02d}_img_%02n.jpg".format(i))
        led[i] = 0


def capture_single():
    led.all_on()
    c.capture()

def download_all_new():
    c.download_and_delete_all()

def led_off():
    led.all_off()

def led_on():
    led.all_on()



canvas = Canvas(root, width=720, height=720)
canvas.pack()
capture_and_display_preview()




#button_focus_reset_to_index = Button(root, text="reset focus to index", command=focus_reset_to_index)
button_focus_near = Button(root, text="focus Near", command=button_focus_near)
button_focus_far = Button(root, text="focus Far", command=button_focus_far)
capture_all_lights = Button(root, text="Capture all 12 lights", command=button_capture_all_lights)
button_capture_single = Button(root, text="Capture", command=capture_single)
#button_download_all_new = Button(root, text="Download and delete all new from SD", command=download_all_new)
button_led_on = Button(root, text="Led on", command=led_on)
button_led_off = Button(root, text="Led off", command=led_off)


focus_offset = Entry(root)
focus_offset.insert(0, "10")
focus_index = Entry(root)
focus_index.insert(0, "5000")
directory_name = Entry(root)
directory_name.insert(0, "outputDirectory")
#button_focus_reset_to_index.pack()
button_focus_near.pack()
button_focus_far.pack()
focus_offset.pack()
focus_index.pack()
directory_name.pack()
capture_all_lights.pack()
button_capture_single.pack()
#button_download_all_new.pack()
button_led_on.pack()
button_led_off.pack()

root.mainloop()

