import array
from ola.ClientWrapper import ClientWrapper

NUM_LED = 12  # LED id and DMX channels start at 1. LedController respects this
UNIVERSE = 1  # configured in OLA directly.


class LedController(object):
    def __init__(self):
        self.universe = UNIVERSE
        self.num_led = NUM_LED
        self.data = array.array('B', [0] * self.num_led)
        self.dmx = ClientWrapper()
        self.all_on()

    def __setitem__(self, i, val):
        self.data[i - 1] = val
        self.update_dmx()

    def __getitem__(self, i):
        return self.data[i - 1]

    def all_off(self):
        self.data = array.array('B', [0] * self.num_led)
        self.update_dmx()

    def all_on(self):
        self.data = array.array('B', [255] * self.num_led)
        self.update_dmx()

    def update_dmx(self):
        self.dmx.Client().SendDmx(self.universe, self.data, callback=self._dmx_stop)
        self.dmx.Run()

    def _dmx_stop(self, *_):
        self.dmx.Stop()


if __name__ == "__main__":
    # manual test
    import time

    led = LedController()
    led.all_on()

    for i in range(30, 255):
        time.sleep(0.1)
        for j in range(12):
            led[j + 1] = i
