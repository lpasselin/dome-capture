import subprocess
import os

def hdr_merge_command(all_input_path, output_path):
    # luminance-hdr always ends in segfault
    # command = "luminance-hdr-cli %s --save \"%s\"" % (all_input_path, output_path)
    # hdrgen
    # command = "hdrgen/bin/hdrgen64bit -o \"%s\" %s" % (output_path, all_input_path)
    # pfstools
    # command = "pfsinme %s | pfsalign -v -c min | pfshdrcalibrate -r linear | pfsoutexr %s" % (all_input_path, output_path)
    command = "pfsin %s | pfsoutexr %s" % (all_input_path, output_path)
    return command

if __name__ == "__main__":
    # DATA_ROOT = "/home/louis/ms/BRDF-Dome/src/img_dump/"
    # DATA_ROOT = "/home/louis/p/brdf-dome-calibration/data/2018-08-16/"
    DATA_ROOT = "/opt/project/data/2018-08-16/"
    folders = os.listdir(DATA_ROOT)
    folders.sort()
    total_folders = len(folders)
    print("Num folders: ",  total_folders)
    # for c, f in enumerate(folders):
    for c, f in enumerate(["chromeball"]):
        folder_path = DATA_ROOT + f + "/"
        print("{}/{}: {}".format(c, total_folders, folder_path))

        for lum in range(1, 13):
            filename_suffix = "light_%02d" % lum

            all_input_path = ' '.join(["\"" + folder_path + filename_suffix + "_img_%02d.jpg\"" % i for i in range(1, 6)])
            # all_input_path = folder_path + filename_suffix + "_img_\%02d.jpg"
            output_path = folder_path + filename_suffix + "_3" + ".exr"
            command = hdr_merge_command(all_input_path, output_path)

            print(command)
            result = subprocess.run(command, shell=True, stdout=subprocess.PIPE)
            print(result.stdout)

            # if this doesn't work, https://docs.opencv.org/3.3.1/d3/db7/tutorial_hdr_imaging.html